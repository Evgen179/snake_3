// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnFood.h"
#include "SnakeGameGameModeBase.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "Food.h"



// Sets default values
ASpawnFood::ASpawnFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnFood::BeginPlay()
{
	Super::BeginPlay();
	AddRandomFood();
}

// Called every frame
void ASpawnFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BufferTime += DeltaTime;
	if (BufferTime > StepDelay)
	{
		AddRandomFood();
		BufferTime = 0;
	}
}

void ASpawnFood::AddRandomFood()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
	
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;

	GetWorld()->SpawnActor<AFood>(FoodClass, StartPoint, StartPointRotation, SpawnParameters);
		
}