// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnFood.generated.h"

class AFood;

UCLASS()
class SNAKEGAME_API ASpawnFood : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnFood();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//�������� ������ ���
	float MinY = -700.f; float MaxY = 700.f;
	float MinX = -700.f; float MaxX = 700.f;
	float SpawnZ = 10.f;
		
	//�������� ���������
	float StepDelay = 4.f;
	//���������� �������
	float BufferTime = 0;
	//������� �������� ��� � ��������� �����
	UFUNCTION()
	void AddRandomFood();
};
